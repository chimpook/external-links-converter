/**
 * Created by s.veselovsky@astramg.ru on 20.04.2016.
 */
window.onload = function() {
    $( "button[name=process]" ).click( function() { extlink.process( this ); });
    $( "button[name=queries]" ).click( function() { extlink.queries( this ); });
    $( "button[name=check]" ).click( function() { extlink.check( this ); });
}

var extlink = {

    process: function( object ) {

        var obj_id = $( object ).attr("data-obj_id");
        var field_id = $( object ).attr("data-field_id");
        var counter = $( object).attr("data-article");
        var content = $( "#article_"+counter+" .content").html();
        //console.log(obj_id + " : " + field_id);
        //console.log(content);

        $(".debug").load(
            "external_link_process.php",
            {
                obj_id: obj_id,
                field_id: field_id,
                content: content
            },
            function(result){

            }

        );


    },
    queries: function( object ) {

        var obj_id = $( object ).attr("data-obj_id");
        var field_id = $( object ).attr("data-field_id");
        var counter = $( object).attr("data-article");
        var content = $( "#article_"+counter+" .content").html();
        //console.log(obj_id + " : " + field_id);
        //console.log(content);

        $(".debug").load(
            "external_link_queries.php",
            {
                obj_id: obj_id,
                field_id: field_id,
                content: content
            },
            function(result){

            }

        );


    },
    check: function( object ) {

        var obj_id = $( object ).attr("data-obj_id");
        var field_id = $( object ).attr("data-field_id");
        var counter = $( object).attr("data-article");
        var content = $( "#article_"+counter+" .content").html();
        //console.log(obj_id + " : " + field_id);
        //console.log(content);

        $(".debug").load(
            "external_link_check.php",
            {
                obj_id: obj_id,
                field_id: field_id,
                content: content
            },
            function(result){

            }

        );
    }

};