<?php

ini_set('display_errors', 1);

$host = 'localhost';
$login = 'margulisru_1000';
$password = 'ghdsau';
$database = 'margulisru_1000';
$table = 'cms3_object_content';
$obj_id_name = 'obj_id';
$field_id_name = 'field_id';
$text_val_name = 'text_val';

echo <<<'Header'
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>External links parser</title>
<script type="text/javascript" src="jquery/jquery.js"></script>
<script type="text/javascript" src="external_link_process.js"></script>
<style>
    .article {
        background: powderblue;
        border-radius: 4px;
        padding: 4px 8px 4px 8px;
        margin-bottom: 4px;
        /*width: 100%;*/
    }
    .article .nav { margin-left: 12px; margin-right: 12px; }
    .article .field_id { margin-right: 12px; }
    .article.corrected {background: lightgreen;}
    .red {color: red;}
    .green {color: green;}
    .hidden-link {background: red; color: white; border-radius: 4px; padding: 2px;}
    .workspace {
        position: absolute;
        top: 12px; bottom: 12px; left: 12px;
        width: calc(50% - 36px); height: calc(100% - 34px);
        background: honeydew;
        overflow: auto;
        padding: 4px;
        border: 1px solid mediumaquamarine;
    }
    .workspace button { margin-right: 8px;}
    .debug {
        position: absolute;
        top: 12px; right: 12px; bottom: 12px;
        width: calc(50% - 24px); height: calc(100% - 124px);
        background: wheat;
        padding: 8px;
        overflow: auto;
    }
    .debug input {
        width: calc(100% - 24px);
        padding: 4px;
    }
    .debug textarea {
        margin-top: 4px;
        width: calc(100% - 24px);
        height: calc(50% - 48px);
        padding: 4px;
    }
    .debug .backup input, .debug .backup textarea { background: powderblue; }
    .debug .process input, .debug .process textarea { background: palegoldenrod; }
    .resume {
        position: absolute;
        bottom: 12px;
        right: 12px; width: calc(50% - 24px); height: 56px;
        background: beige;
        padding: 8px;
    }
    .resume table { width: calc(100% - 24px); }
    .resume table td { text-align: center; }

</style>
</head>
<body>
Header;

/**
 * test001
 */
/*
$string = '<a href="www.ria.ru/article/1">Article 1</a>';
$addr = 'www.ria.ru/article/1';
echo "<xmp>$string</xmp>";
echo '<br />';
echo str_replace("/", "\/", $addr);
$string = preg_replace('/<a href="'.str_replace("/", "\/", $addr).'">(.*)<\/a>/', '<span class="hidden-link" data-link="'.$addr.'">$1</span>', $string);
echo "<xmp>$string</xmp>";
*/


/**
 * test002
 */
/*
$content = '<p>Статья за номером 1: <a href="http://1000kadrov.ru/article/1">Относительно энтропии</a></p>'."\n" .
    '<p>Статья за номером 2: <a href="http://ria.ru/article/112">Внешняя 222</a></p>'."\n".
    '<p>Дополнение: <a href="https://www.1000kadrov.ru/title/34">Обратно</a></p>'."\n".
    '<p>Комментарий: <a href="http://test.com/proverka?34">Внешняя 123</a></p>'."\n".
    '<p>Комментарий: <a href="http://test.com/proverka?35">Внешняя 11</a></p>'."\n".
    '<p>Дополнение: <a href="https://www.1000kadrov.ru/title/34">Обратно</a></p>'."\n";

echo '<div class="article">'.$content.'</div>';

preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $matches);

$i=1;
foreach($matches[0] as $match) {
    $url = parse_url($match);
    if ( strcmp($url['host'], '1000kadrov.ru')  && strcmp($url['host'], 'www.1000kadrov.ru') )
    {
        echo "\n".'<p>' . $i++ . ' : ';
        echo '<span class="red">'. $match .'</span> : ';
        echo str_replace('/', '\/', preg_quote($match));
        echo '</p>';
        $a = preg_replace('/<a href="'.str_replace('/', '\/', preg_quote($match)).'">(.*)<\/a>/', '<span class="hidden-link" data-link="'.$match.'">$1</span>', $content);
        $content = null;
        $content = $a;
    }
}
echo '<hr />';
echo $content;
*/

echo '<div class="debug"></div>';
echo '<div class="workspace">';
$link = new mysqli($host, $login, $password, $database) or die("@" . __FILE__ . "@" . __LINE__ . " : " . $link->error);
$link->query("SET NAMES 'utf8'") or die("@" . __FILE__ . "@" . __LINE__ . " : " . $link->error);

$result = $link->query("SELECT * FROM `".$table."` WHERE `{$text_val_name}` RLIKE '.*href=.*' LIMIT 1000") or die("@" . __FILE__ . "@" . __LINE__ . " : " . $link->error);

$number = $result->num_rows;
$articles_with_links_counter = $number;
$external_links_counter = 0;
$articles_with_external_links_counter = 0;

$external_links = array();
for ($i=0; $i<$number; $i++) {
    $row = $result->fetch_array();
    $content = $row[$text_val_name];
    $obj_id = $row[$obj_id_name];
    $field_id = $row[$field_id_name];
    if ($content) {
        preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $matches);
        $flag = false;
        foreach($matches[0] as $match) {
            $url = parse_url($match);
            if ( strcmp($url['host'], '1000kadrov.ru')  && strcmp($url['host'], 'www.1000kadrov.ru') )
            {
                /*
                $a = preg_replace(
                    array('/<a href="'.str_replace('/', '\/', preg_quote($match)).'">(.*)<\/a>/',
                        '/<a href="'.str_replace('/', '\/', preg_quote($match)).'" target="_blank">(.*)<\/a>/'
                        ), '<span class="hidden-link" data-link="'.$match.'">$1</span>', $content);
                */
                //$a = str_replace('<br />', '', $content);
                $a = preg_replace(
                    array('#<a href="'.preg_quote($match).'">(.*)</a>#',
                        '#<a href="'.preg_quote($match).'" target="_blank">(.*)</a>#',
                        '#<a target="_blank" href="'.preg_quote($match).'">(.*)</a>#'
                    ), '<span class="hidden-link" data-link="'.$match.'">$1</span>', $content,-1,$replacements_counter);
                if ($replacements_counter) {
                    $content = null;
                    //$content = $link->real_escape_string($a);
                    $content = $a;
                    $flag = true;
                    $external_links_counter++;
                }
                $external_links[] = $match;
            }
        }
        if ($flag){
            $articles_with_external_links_counter++;
            echo '<div id="article_'.$articles_with_external_links_counter.'" class="article corrected">'.
                    '<h3>'.$articles_with_external_links_counter.' : '. ($i+1) .'</h3>'.
                    '<span class="obj_id">'.$obj_id.'</span>:<span class="field_id">'.$field_id.'</span>'.
                    '<button name="process" data-article="'.$articles_with_external_links_counter.'" data-obj_id="'.$obj_id.'" data-field_id="'.$field_id.'">Конвертировать</button>'.
                    '<button name="queries" data-article="'.$articles_with_external_links_counter.'" data-obj_id="'.$obj_id.'" data-field_id="'.$field_id.'">Запросы</button>'.
                    '<button name="check" data-article="'.$articles_with_external_links_counter.'" data-obj_id="'.$obj_id.'" data-field_id="'.$field_id.'">Проверить</button>'.
                    '<a class="nav" href="#article_'.($articles_with_external_links_counter+1).'">далее</a>'.
                    '<hr />'.
                    '<span class="content">'.
                    $content.
                    '</span>'.
                 '</div>';
        }
        else {
            //echo '<div class="article">'.$content.'</div>';
        }
    }
}
echo '</div>';

echo <<< RESUME
<div class="resume">
<table>
<tr><th colspan="3">ИТОГО</td></tr>
<tr>
    <td>Записей со ссылками: <strong>{$articles_with_links_counter}</strong></td>
    <td>Записей с внешними ссылками : <strong>{$articles_with_external_links_counter}</strong></td>
    <td>Всего внешних ссылок : <strong>{$external_links_counter}</strong></td>
</tr>
</table>
</div>
RESUME;


/*
sort($external_links);
foreach($external_links as $counter=>$link) {
    echo "\n<br/>".($counter+1).": {$link}";
}
*/
echo <<<'Footer'
</body>
</html>
Footer;
